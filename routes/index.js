var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', 
  { 
    title: 'Express',
    welcome: 'this site'
  }); // render looks in views
});

module.exports = router;
