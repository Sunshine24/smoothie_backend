/*Catalog de smoothie */
var express = require("express");
const Smoothie = require('../models/smoothie');
const {
    check,
    validationResult
} = require('express-validator');

const sanitizeBody = require('express-validator');

var router = express.Router();

/* GET users listing. */
router.get("/list", async (req, res, next) => {

    try{
        const smoothies = await Smoothie.find({});
        console.log('Liste des smoothie : ',smoothies);
        res.send(smoothies);
    } catch(err) {
        console.log(err);
        res.status(400).send(err);
    }

});

router.post('/add', [
    check('title').not().isEmpty().withMessage("Le nom ne peut être vide"),
], async (req, res, next) => {

        // Validation du formulaire
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).send({
                errors: errors.array()
            });
            return;
        }

    let newSmoothie = new Smoothie();
    newSmoothie.title = req.body.title;

    try {
        const smoothie = await newSmoothie.save();
        res.send(smoothie);
    } catch(err) {
        res.status(400).send(err);
    }
});

router.get('/:id', async (req, res, next) =>{
    try{
        const smoothies = await Smoothie.findById(req.params.id).exec();
        console.log('Liste des smoothie : ',smoothies);
        res.send(smoothies);
    } catch(err) {
        console.log(err);
        res.status(400).send(err);
    }


});
module.exports = router;



