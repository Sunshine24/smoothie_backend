// to edit one smoothie

// import mongoosse
const mongoose = require("mongoose");

const smoothieSchema = mongoose.Schema({
    title: {
        type: String,
        required: false
    },
    ingredients: [
        {
            nom: {
                type: String
            },
            quantite: {
                type: String
            }
        },           
        {
            nom: {
                type: String
            },
            quantite: {
                type: String
            } 
        }
    ],
    features: {
        cost: {
            type: String
        },
        prepareTime: {
            type: String
        }
    },
    advice: {
        type: String
    },
    description: {
        type: String
    },
    steps: [
        {
            stepText: {
                type: String
            }
        },
        
    ],
    photo: [
    {
        title: String,
        path: String,
        description: String
    }
    ]
});

const Smoothie = module.exports = mongoose.model('smoothie', smoothieSchema);

